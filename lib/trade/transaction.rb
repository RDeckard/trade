module Trade
  class Transaction
    extend Forwardable

    STATUSES = %i[pending bought selled].freeze

    attr_reader :product, :amount, :status, :bought_at, :selled_at

    def_delegators :@product, :order_cost

    def initialize(product)
      @product = product
      @amount = 1
      self.status = :pending
    end

    def buy!(amount, time = Time.now)
      if status == :pending
        @amount = amount
        @bought_at = time
        self.status = :bought
      else
        raise "`status` value must be 'pending' to buy"
      end
    end

    def sell!(time = Time.now)
      if status == :bought
        @selled_at = time
        self.status = :selled
      else
        raise "`status` value must be 'bought' to sell"
      end
    end

    def value(time = Time.now, force_ask_value: false)
      (
        (
          (status == :pending or force_ask_value) ? product.ask_price(time) : product.bid_price(time)
        ) * amount
      ).round(2)
    end

    def profit(time = Time.now, force_ask_value: false)
      if status == :pending
        0.0
      else
        (
          value(time, force_ask_value: force_ask_value) -
          value(bought_at, force_ask_value: true)
        ).round(2)
      end
    end

    def bought_value
      bought_at && value(bought_at, force_ask_value: true)
    end

    def selled_value
      selled_at && value(selled_at)
    end

    def selled_profit
      selled_at && profit(selled_at)
    end

    def infos
      {
        product: product.infos,
        amount: amount,
        status: status,
        bought_at: bought_at,
        bought_value: bought_value,
        selled_at: selled_at,
        selled_value: selled_value,
        selled_profit: selled_profit,
        value: value,
        profit: profit
      }
    end

    private

    def status=(new_status)
      if STATUSES.include? new_status&.to_sym
        @status = new_status.to_sym
      else
        raise "`status` value must be in #{STATUSES}"
      end
    end
  end
end
