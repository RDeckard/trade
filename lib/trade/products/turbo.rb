module Trade
  module Products
    class Turbo
      using Trade::Refinements
      extend Forwardable

      TYPES = %i[call put].freeze

      attr_reader :type, :broker, :created_at, :base_leverage, :chart_strike_price, :parity, :spread

      def_delegators :@broker, :order_cost, :chart_value

      def initialize(type, broker, created_at: Time.now, base_leverage:, parity: 1)
        self.type = type
        @broker = broker
        @created_at = created_at
        @base_leverage = base_leverage.to_f.round(2)
        @chart_strike_price = (
          chart_value(created_at).send(
            type == :call ? :- : :+,
            chart_value(created_at) / base_leverage)
        ).round(3)
        @parity = parity
        @spread = ask_price(created_at) * broker.spread
      end

      def ask_price(time = Time.now)
        (
          (chart_value(time) - chart_strike_price) /
          parity
        ).abs.round(5)
      end

      def bid_price(time = Time.now)
        (ask_price(time) - spread).round(5)
      end

      def leverage(time = Time.now)
        (
          chart_value(time) /
          (chart_value(time) - chart_strike_price)
        ).abs.round(2)
      end

      def infos
        {
          general: {
            class: self.class,
            type: type,
            broker: broker.infos(with_products: false),
            created_at: created_at.to_minute,
            base_chart_value: chart_value(created_at),
            base_leverage: base_leverage,
            chart_strike_price: chart_strike_price,
            parity: parity
          },
          ask_price: ask_price,
          bid_price: bid_price,
          leverage: leverage,
          chart_value: chart_value
        }
      end

      private

      def type=(new_type)
        if TYPES.include? new_type&.to_sym
          @type = new_type.to_sym
        else
          raise "`type` value must be in #{TYPES}"
        end
      end
    end
  end
end
