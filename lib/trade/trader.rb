module Trade
  class Trader
    using Trade::Refinements

    TIME_STEP = 60

    attr_reader :broker, :account, :target_leverage, :max_loss, :strategy, :verbose, :transactions, :time, :threshold

    def initialize(broker, account: 10000, target_leverage: 30, max_loss: 0.01, strategy: :random, verbose: false)
      raise "Broker's charts are invalid !" if broker.invalid_charts?
      @broker = broker
      self.account = account
      @target_leverage = target_leverage
      @max_loss = max_loss
      @strategy = strategy
      @verbose = verbose
      @transactions = []
      @time = broker.charts.first.minute.to_time
    end

    def work!
      account_at_start = account
      while account > 0 and not broker.last_chart?(time) and (transaction = new_transaction)
        puts "#{time.to_minute} - account: #{account}$" if verbose
        trade new_transaction
      end
      puts "#{time.to_minute} - account: #{account}$" if verbose
      puts "final profit: #{(account - account_at_start).round(2)}$" if verbose
    end

    def infos
      {
        account: account,
        broker: broker.infos,
        transactions: transactions.map(&:infos),
        time: time
      }
    end

    def new_transaction
      targeted_product_type = strategy == :random ? %i[call put].sample : strategy
      broker.products.find{|p| p.leverage(time) >= target_leverage and p.type == targeted_product_type}&.
        then { |product| Transaction.new(product) }&.
        then { |transaction| @transactions << transaction }&.
        then(&:last)
    end

    def trade(transaction)
      return unless transaction

      self.account -= transaction.order_cost
      amount = (account / transaction.product.ask_price(time)).to_i
      self.account -= amount * transaction.product.ask_price(time)
      transaction.buy!(amount, time)
      puts "  buy #{transaction.amount} of '#{transaction.product.broker.symbol}' (:#{transaction.product.type}) for #{transaction.bought_value}$ (order cost: #{transaction.order_cost}$)" if verbose

      self.threshold = transaction.bought_value - transaction.bought_value * max_loss
      @higher_transaction_value = transaction.value(time)

      puts state_of(transaction) if verbose
      begin
        @time += TIME_STEP
      end until broker.last_chart?(time) or analyse(transaction) == :sell!

      transaction.sell!(time)
      transaction_final_value = [threshold, transaction.selled_value].max
      self.account += transaction_final_value - transaction.order_cost
      puts "  sell all for #{transaction_final_value}$ (profit: #{(account - transaction.bought_value).round(2)}$, order cost: #{transaction.order_cost}$)" if verbose
    end

    def analyse(transaction)
      delta = transaction.value(time) - transaction.value(time - TIME_STEP)
      puts state_of(transaction) if verbose
      if delta < 0
        transaction.value(time) < threshold ? :sell! : :keep
      else
        threshold_delta = transaction.value(time) - @higher_transaction_value
        if threshold_delta > 0
          @higher_transaction_value = transaction.value(time)
          self.threshold += threshold_delta
        end
        :keep
      end
    end

    private

    def account=(new_account)
      @account = new_account.round(2)
    end

    def threshold=(new_threshold)
      @threshold = new_threshold.round(2)
    end

    def state_of(transaction)
      return unless transaction
      "    #{time.to_minute}: #{transaction.profit(time)}$" \
      " (value: #{transaction.value(time)}," \
      " threshold: #{'!!!' if transaction.value(time) < threshold}#{threshold}$#{'!!!' if transaction.value(time) < threshold}," \
      " spread cost: #{(transaction.profit(time, force_ask_value: true) - transaction.profit(time)).round(2)}$)"
    end
  end
end
