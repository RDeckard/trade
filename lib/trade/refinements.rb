module Trade
  module Refinements
    refine Time do
      def to_minute
        strftime('%H:%M')
      end
    end

    refine String do
      def to_time
        hour, minute = split(':').map(&:to_i)
        Time.new(Time.now.year, Time.now.month, Time.now.day, hour, minute)
      end
    end
  end
end
