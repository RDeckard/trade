module Trade
  class Broker
    using Trade::Refinements

    attr_reader :symbol, :product_classes, :date, :products
    attr_accessor :order_cost, :spread

    def initialize(symbol,
                   product_classes: [::Trade::Products::Turbo],
                   order_cost: 0,
                   spread: 0.01,
                   date: Date.today - 1)
      @symbol = symbol
      @product_classes = product_classes
      @order_cost = order_cost
      @spread = spread
      @date = date
    end

    def products
      product_classes.flat_map do |product_class|
        5.step(200,5).flat_map do |targeted_leverage|
          [
            product_class.new(:call, self, created_at: charts.first.minute.to_time, base_leverage: targeted_leverage),
            product_class.new(:put, self, created_at: charts.first.minute.to_time, base_leverage: targeted_leverage)
          ]
        end
      end
    end

    def charts
      @charts ||=
        if symbol[/\//]
          Trade.cache.fetch(symbol.sub('/', '-'), max_age: 12 * 3600) do
            Alphavantage::ForEx::Chart.get(%i[from to].zip(symbol.split('/')).to_h)
          end
        else
          Trade.cache.fetch("#{symbol}_#{date}", max_age: 12 * 3600) do
            IEX::Resources::Chart.get(symbol, date).sort_by(&:minute)
          end
        end
    end

    def invalid_charts?
      charts.empty? or charts.first.average < 0 or charts.last.average < 0
    end

    def chart_value(time = Time.now)
      minute = time.to_minute
      # chart = charts.bsearch{ |c| minute <=> c.minute }
      chart = charts.find{ |c| c.minute == minute }
      if chart
        if chart.average < 0
          chart_index = charts.find_index(chart)
          begin
            chart_index -= 1
            chart = charts[chart_index]
          end while chart.average < 0
        end
        chart.average.to_f
      else
        chart_value(charts.last.minute.to_time)
      end
    end

    def last_chart?(time = Time.now)
      minute = time.to_minute
      charts.find_index{ |c| c.minute == minute } == charts.count - 1
    end

    def infos(with_products = true)
      {
        symbol: symbol,
        date: date,
        charts_count: charts.count,
        products_count: products.count
      }
    end
  end
end
