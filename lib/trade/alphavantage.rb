require 'httparty'
require 'ostruct'

module Alphavantage
  class Error < StandardError; end

  module ForEx
    module Chart
      extend self

      ALPHAVANTAGE_DOMAIN = 'https://www.alphavantage.co'

      def get(from:, to:, type: 'intraday', interval: '1min', outputsize: 'full', verbose: false)
        raise StandardError.new "No 'ALPHAVANTAGE_APIKEY' found !" unless ENV['ALPHAVANTAGE_APIKEY']

        query = "function=FX_#{type.upcase}&from_symbol=#{from}&to_symbol=#{to}&interval=1min&outputsize=full"
        url = "#{ALPHAVANTAGE_DOMAIN}/query?#{query}&apikey=#{ENV['ALPHAVANTAGE_APIKEY']}"
        puts "\n#{url}\n" if verbose

        begin
          response = HTTParty.get(url)
        rescue => e
          raise RuntimeError.new "Failed request: #{e.message}"
        end
        data = response.body
        begin
          data = JSON.parse(data)
        rescue
          raise RuntimeError.new message: "Parsing failed", data: data
        end

        if data["Error Message"]
          raise Alphavantage::Error.new data["Error Message"]
        elsif data["Information"]
          raise Alphavantage::Error.new data["Information"]
        end

        data.values.last.map do |k, v|
          OpenStruct.new({
            minute:  k[/\d{2}:\d{2}/],
            high:    v.values[1].to_f,
            low:     v.values[2].to_f,
            average: v.values[3].to_f
          })
        end.reverse
      end
    end
  end
end
