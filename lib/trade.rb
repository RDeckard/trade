require 'bundler/setup'
require 'dotenv/load'
require 'pry'

require 'iex-ruby-client'

require 'fcaching'

require_relative 'trade/alphavantage'
require_relative 'trade/refinements'

require_relative 'trade/broker'
products_directory = File.join(Dir.pwd, 'lib', 'trade', 'products')
Dir.each_child(products_directory) do |file|
  require File.join(products_directory, file)
end
require_relative 'trade/transaction'
require_relative 'trade/trader'

module Trade
  def self.cache
    @cache ||= FCaching.new
  end
end

module Tests
  extend self
  DEVISES = %w[EUR USD GBP CAD AUD].freeze
  SYMBOLS =
    DEVISES.flat_map{ |d1|
      DEVISES.
        reject{|d2| d2 == d1}.
        map{ |d2|
          [d1, d2]
        }
    }.map{|pair| pair.join('/')}.freeze
  # SYMBOLS = JSON.parse(File.read('symbols.json')).select{|h| h['isEnabled']}.map{|h| h['symbol']}.uniq.sample(50).freeze
  # SYMBOLS = ['MSFT']
  # DAYS_BEFORE_TODAY = 7.downto(1).freeze
  DAYS_BEFORE_TODAY = [1].freeze
  SPREADS = [0.002].freeze
  ORDER_COSTS = [0].freeze
  BASE_ACCOUNTS = [10000].freeze
  TARGET_LEVERAGES = [30].freeze
  MAX_LOSSES = [0.01, 0.02, 0.05, 0.1].freeze
  STRATEGIES = Array.new(5, :random).freeze
  # STRATEGIES = [:call, :put, :random].freeze

  def batch(option = nil)
    stats = {}
    SYMBOLS.each do |symbol|
      puts "symbol: #{symbol}"
      DAYS_BEFORE_TODAY.each do |days_before_today|
        puts "  days_before_today: #{days_before_today}" unless option == :fetch
        broker = Trade::Broker.new(symbol, date: Date.today - days_before_today)
        next if broker.invalid_charts?
        next if option == :fetch
        SPREADS.each do |spread|
          puts "    spread: #{spread}"
          broker.spread = spread
          ORDER_COSTS.each do |order_cost|
            puts "      order_cost: #{order_cost}"
            broker.order_cost = order_cost
            BASE_ACCOUNTS.each do |base_account|
              puts "        base_account: #{base_account}"
              TARGET_LEVERAGES.each do |target_leverage|
                puts "          target_leverage: #{target_leverage}"
                MAX_LOSSES.each do |max_loss|
                  puts "            max_loss: #{max_loss}"
                  STRATEGIES.each do |strategy|
                    puts "              strategy: #{strategy}"
                    trader = Trade::Trader.new(broker, account: base_account, target_leverage: target_leverage, max_loss: max_loss, strategy: strategy)
                    next unless broker.products.find{|p| p.leverage(trader.time) >= trader.target_leverage}
                    trader.work!
                    stat_key = {
                        settings: {
                          spread: spread,
                          order_cost: order_cost,
                          base_account: base_account,
                          target_leverage: target_leverage,
                          max_loss: max_loss,
                          strategy: strategy
                        }
                      }
                    stat_value = stats[stat_key]
                    if stat_value
                      stat_value[:days] = stat_value[:days] + 1
                      stat_value[:total_profit] = (stat_value[:total_profit] + trader.account - base_account).round(2)
                      stat_value[:average_profit_by_day] = (stat_value[:total_profit] / (stat_value[:contexts].count + 1)).round(2)
                      stat_value[:contexts] <<
                        {
                          symbol: symbol,
                          days_before_today: days_before_today,
                          profit: (trader.account - base_account).round(2),
                        }
                    else
                      stat_value = {
                          days: 1,
                          total_profit: (trader.account - base_account).round(2),
                          average_profit_by_day: (trader.account - base_account).round(2),
                          contexts: [
                            {
                              symbol: symbol,
                              days_before_today: days_before_today,
                              profit: (trader.account - base_account).round(2)
                            }
                          ]
                        }
                    end
                    stats.merge!({stat_key => stat_value})
                  end
                end
              end
            end
          end
        end
      end
    end
    stats = stats.sort_by{|_, v| v[:profit]}
    binding.pry unless option == :fetch
  end

  def one(symbol)
    trader =
      Trade::Trader.new(
          Trade::Broker.new(
              (symbol || SYMBOLS.first),
              spread: SPREADS.first,
              order_cost: ORDER_COSTS.first,
              date: (Date.today - DAYS_BEFORE_TODAY.first)
            ),
          account: BASE_ACCOUNTS.first,
          target_leverage: TARGET_LEVERAGES.first,
          max_loss: MAX_LOSSES.first,
          strategy: STRATEGIES.first,
          verbose: true
        )
    trader.work!
  end

  def pry
    broker = Trade::Broker.new(SYMBOL)
    turbo = Trade::Products::Turbo.new(:call, broker, base_leverage: 20)
    transaction = Trade::Transaction.new(turbo)

    binding.pry
  end
end

case ARGV[0]
when 'fetch'
  begin
    Tests.batch(:fetch)
  rescue Alphavantage::Error => e
    puts "Alphavantage::Error: \"#{e.message}\""
    puts "Retry in 60 seconds..."
    sleep 60
    retry
  end
when 'batch'
  Tests.batch
when 'one'
  Tests.one(ARGV[1])
when 'pry'
  Tests.pry
end
